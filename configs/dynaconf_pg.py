import configparser
import logging

logger = logging.getLogger(__name__)

def read_config(file_name: str):
    """Reads and returns content of config file."""
    parser = configparser.ConfigParser()
    parser.read(file_name)
    return parser


def write_config(section: str, name: str, ip: str, conf_file: str):
    """
    Takes a (new) section and the value of its two keys and writes to given file.
    Returns the updated config object.
    
    """
    conf = read_config(conf_file)
    conf.add_section(section=section)
    conf.set(section, "Name", name)
    conf.set(section, "IP", ip)

    with open(conf_file, "w") as the_file:
        conf.write(the_file)
    
    return read_config(the_file)


def main(conf_file: str):
    logger.info(f"Running main()")
    conf = read_config(conf_file)

    # Sections
    secs = [*conf.sections()]
    print(f"Sections: {secs}, type: {type(secs)}.")

    for s in conf.sections():
        print(f"Section: {s}")
        print(f"Name: {conf[s]['Name']}")
        print(f"IP: {conf[s]['IP']}:{conf[s]['Port']}\n")
    
    for i in conf.items():
        print(f"{i}, type: {type(i)}.")



if __name__ == "__main__":
    main("config.ini")

    write_config("node04", "node04", "10.0.40.1", "config.ini")
    main("config.ini")

